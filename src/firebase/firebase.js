import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyC0Fw0yDiy7ffXxGwYb26BzA2eSVqa-0q8",
    authDomain: "fiesta-demo-app.firebaseapp.com",
    databaseURL: "https://fiesta-demo-app.firebaseio.com",
    projectId: "fiesta-demo-app",
    storageBucket: "fiesta-demo-app.appspot.com",
    messagingSenderId: "713575987516"
  };

  const Firebase = firebase.initializeApp(config);

  export default Firebase;
  