import React from 'react';
import ReactPaginate from 'react-paginate';
import './pagination.scss';

export default (props) => (
    <ReactPaginate
        // previousLabel={'previous'}
        previousLabel={'WIP'}
        // nextLabel={'next'}
        nextLabel={'WIP'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={3}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={props.onClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
    />
);
