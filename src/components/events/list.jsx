import React from 'react';
import ListItem from './list-item';
import './list.scss';

export default (props) => {
    const items = props.data.map(item => {
        return <ListItem key={item.id} event={item} handleClick={props.handleClick}/>;
    });

    return (
        <section className='event-list'>
            {items}
        </section>
    );
}