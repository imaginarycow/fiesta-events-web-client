import React from 'react';
import Button from 'react-bootstrap/Button';

export default (props) => {
    return (
        <section>
            <h2 className='fiesta'>{props.title}</h2>
            <Button onClick={props.handleClick} variant="success" >ADD NEW EVENT</Button>
        </section>
    )
}