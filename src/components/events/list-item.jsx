import React from 'react';
import Button from 'react-bootstrap/Button';
import './list-item.scss';


export default (props) => {
    const event = props.event;
    return (
        <section className='list-item'>
            <span><label>Event</label> <br/>{event.name}</span>
            <span><label>Location</label><br/>{event.location.name}</span>
            <span><label>Date</label><br/>{event.date}</span>
            <span><label>Starts</label><br/>{event.start}</span>
            <span><label>Ends</label><br/>{event.stop}</span>
            <span><label>Price</label><br/>{parseFloat(event.price).toFixed(2)}</span>
            <span style={{width:'100%'}}>
                <Button variant="info" onClick={() => props.handleClick(props.event)}>Edit | Delete</Button>
            </span>
            
        </section>
    )
}