import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';


// custom imports
import List from './list';
import ListHeader from './list-header';
import Pagination from '../pagination/pagination-bar';
import './list.scss';

class EventContainer extends React.Component {

    constructor() {
        super();

        this.state = { eventsPerPage: 5, currentPage: 1, redirect: null, redirectProps: null };

        this.addNewEvent = this.addNewEvent.bind(this);
        this.editEvent = this.editEvent.bind(this);
        this.getEventsForPage = this.getEventsForPage.bind(this);
        this.handlePageClick = this.handlePageClick.bind(this);
    }

    addNewEvent(event) {
        this.setState({ redirect: '/event/edit/new' });
    }

    editEvent(event) {
        this.setState({ redirect: `/event/edit/${event.name}`, redirectProps: event });
    }

    getEventsForPage(page) {
        
        return this.props.events;
    }

    handlePageClick(click) {
        console.log(click);
    }

    showPagination() {
        return (
            <div id="react-paginate">
                <Pagination onClick={this.handlePageClick}/>
            </div>
        )
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={{
                pathname: this.state.redirect,
                state: this.state.redirectProps
            }}/>
        }
        return (
            <div className='events-container'>
                <ListHeader title='Events' handleClick={this.addNewEvent} actionText='Add New Event'/>
                <hr />
                <List data={this.getEventsForPage(this.state.currentPage)} handleClick={this.editEvent}/>
                {this.props.events.length > this.state.eventsPerPage-1 ? this.showPagination() : null}
            </div>
        )
    }
    
}

const mapStateToProps = state => {
    return { events: state.events.user };
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EventContainer);