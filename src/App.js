import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { BeatLoader } from 'react-spinners';

import loginAnonymously from './redux/actions/login-anon';
import { Routes } from './routes/route-config';

import 'jquery';
import 'bootstrap';

import './App.scss';

class App extends Component {

  componentWillMount() {
    // Future enhancement should support login with oAuth, email, etc.
    // after login, get user events is triggered
    this.props.loginAnonymously();
  }

  render() {

    const loader = (
      <div style={{position: 'absolute', top: '45%'}}>
        <BeatLoader
          sizeUnit={"px"}
          size={30}
          color={'#EA564B'}
          loading={this.props.loading}
        />
      </div>
    )
    return (
      <div className="App">
        <header className="App-header fiesta">
          Fiesta 2019
        </header>
        <div className='app-container'>
          {loader}
          <Routes />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { loading: state.loading };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({loginAnonymously}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);