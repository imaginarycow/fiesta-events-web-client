import { generateEventId } from '../util/uuid-generator';

// will serve as the model for new events
class Event {
    constructor(date, name, location=new Location(null, null, null), price, start, stop) {
        this.date = date;
        this.id = generateEventId(name);
        this.name = name;
        this.location = location;
        this.price = price;
        this.start = start;
        this.stop = stop;
    }
}

class Location {
    constructor(name, lat, long) {
        this.name = name;
        this.lat = lat;
        this.long = long;
    }
}

export {Event, Location};