// will serve as the user model
class User {
    constructor(anonymous, email, id, imgUrl) {
        this.anonymous = anonymous;
        this.email = email;
        this.id = id;
        this.img = imgUrl;
        // And other user properties
        // such as preferences
    }
}

export default User;