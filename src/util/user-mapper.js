import User from '../data/model-user';

function mapAnonUser(user) {
    
    // mapping logic simpified for mvp
    return new User(true, user.email, user.uid, user.photoUrl);
}

// stubs for future
function mapEmailUser(user) {

    let u = new User();

    // mapping logic here

    return u;
}

function mapFacebookUser(user) {

    let u = new User();

    // mapping logic here

    return u;
}

function mapGoogleUser(user) {

    let u = new User();

    // mapping logic here

    return u;
}

function mapTwitterUser(user) {

    let u = new User();

    // mapping logic here

    return u;
}

export { mapAnonUser, mapEmailUser, mapFacebookUser, mapGoogleUser, mapTwitterUser };