import toastr from 'toastr';
import 'toastr/toastr.scss';

const types = {
    success: 'success',
    info: 'info',
    warning: 'warning',
    error: 'error'
}

function toast(msg, type) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      switch(type) {
          case types.error:
            toastr.error(msg);
            break;
        case types.info:
            toastr.info(msg);
            break;
        case types.success:
            toastr.success(msg);
            break;
        case types.warning:
            toastr.warning(msg);
            break;
        default:
            toastr.success(msg);
      }
}

export { types, toast };