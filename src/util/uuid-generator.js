const uuid = require('uuid/v1');
const uuidv3 = require('uuid/v3');
const namespace = '846f3ce0-63af-11e9-9254-4ff33289d185';

function generateRandomUUID() {
    return uuid();
}

function generateEventId(eventName) {
    if (!eventName) {
        return null;
    }
    return uuidv3(eventName, namespace);
}

export {generateRandomUUID, generateEventId};
