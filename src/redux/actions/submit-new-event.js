import firebase from '../../firebase/firebase';
import { toast, types } from '../../util/toastr';
import getUserEvents from './get-user-events';
import setLoading from './set-loading-status';


const submitNewEvent = (userId, event) => {
    return dispatch => {
        dispatch(setLoading(true));
        firebase.database().ref('users/' + userId + '/events/' + event.id).set({
            ...event
        })
        .then(() => {
            toast('Saved Your Fiesta Event!', types.success);
            dispatch(getUserEvents(userId));
        })
        .catch(err => {
            toast('Whoa, something went wrong. Please try that again.', types.warning);
        })
        .finally(() => {
            dispatch(setLoading(false));
        });
    }
}

export default submitNewEvent;