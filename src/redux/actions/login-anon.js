import firebase from '../../firebase/firebase';
import getUserEvents from './get-user-events';
import { mapAnonUser } from '../../util/user-mapper';
import { toast, types } from '../../util/toastr';
import Types from '../action-types';

const setUser = (user) => {

    return {
        type: Types.SET_USER,
        payload: mapAnonUser(user)
    }
}

const LoginAnonymously = () => {

    return dispatch => {
        firebase.auth().signInAnonymously()
        .then(res => {
            // set user in store
            dispatch(setUser(res.user));
            // get user submitted events
            dispatch(getUserEvents(res.user.uid));
        })
        .catch(function(error) {
            toast('There seems to be a disconnect in the matrix. Please check your internet connection!', types.error);
        });
    }
}

export default LoginAnonymously;