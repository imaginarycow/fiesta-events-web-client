import firebase from '../../firebase/firebase';
import setLoading from './set-loading-status';
import Types from '../action-types';

const setUserEvents = (events) => {
    // convert object to array
    let arr = Object.keys(events).map(key => events[key]);
    arr.sort((a, b) => {
        return new Date(a.date) - new Date(b.date);
    });
    return {
        type: Types.SET_USER_EVENTS,
        payload: arr
    }
}

const getUserEvents = (userId) => {
    return dispatch => {
        dispatch(setLoading(true));
        firebase.database().ref('/users/' + userId + '/events').once('value')
        .then(function(snapshot) {
            dispatch(setUserEvents(snapshot.val()));
        })
        .catch(err => {
            // TODO: come back and do something
        }).finally(() => {
            dispatch(setLoading(false));
        })
    }
}

export default getUserEvents;