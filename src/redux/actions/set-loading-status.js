import Types from '../action-types';

const setLoadingStatus = (status) => {

    return {
        type: Types.SET_LOADING_STATUS,
        payload: status
    }
}

export default setLoadingStatus;