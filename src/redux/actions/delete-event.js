import firebase from '../../firebase/firebase';
import { toast, types } from '../../util/toastr';
import getUserEvents from './get-user-events';
import setLoading from './set-loading-status';


const deleteEvent = (userId, eventId) => {
    return dispatch => {
        dispatch(setLoading(true));
        var updates = {};
        updates['/users/' + userId + '/events/' + eventId] = null;
        firebase.database().ref().update(updates)
        .then(() => {
            toast('Say goodbye to your Fiesta Event!', types.success);
            dispatch(getUserEvents(userId));
        })
        .catch(err => {
            toast('Whoa, something went wrong. Please try that again.', types.error);
        })
        .finally(() => {
            dispatch(setLoading(false));
        });
    }
}

export default deleteEvent;