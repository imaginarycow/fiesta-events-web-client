// The combined Reducer
import { combineReducers } from 'redux';
import events from './reducer-events';
import loading from './reducer-loading';
import user from './reducer-user';

export default combineReducers({ 
    events,
    loading,
    user 
});