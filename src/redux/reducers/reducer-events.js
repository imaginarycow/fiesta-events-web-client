import Types from '../action-types';

const defaultState = {
    all: [],  // all available events
    user: [] // user defined events
}

export default function EventsReducer(state=defaultState, action) {
    switch(action.type) {
        case Types.SET_ALL_AVAILABLE_EVENTS:
            return {...state, all: action.payload};
        case Types.SET_USER_EVENTS:
            return {...state, user: action.payload}; 
        default:
            return state;
    }
}