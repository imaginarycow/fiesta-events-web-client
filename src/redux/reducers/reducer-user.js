import Types from '../action-types';

// setting defaultUser to null explicitly
// for confirmation of successful login

export default function(state=null, action) {
    switch(action.type) {
        case Types.SET_USER:
            return action.payload; 
        default:
            return state;
    }
}