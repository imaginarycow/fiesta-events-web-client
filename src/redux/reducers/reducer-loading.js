import Types from '../action-types';

export default function LoadingIndicatorReducer(state=false, action) {
    switch(action.type) {
        case Types.SET_LOADING_STATUS:
            return action.payload; 
        default:
            return state;
    }
}