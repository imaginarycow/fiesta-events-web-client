import React from 'react';
import { Route, Redirect, Switch } from 'react-router';

import home from './home';
import eventEdit from './event-edit';


const routePaths = {
    home: '/',
    eventDetails: '/event/details/:event',
    eventEdit: '/event/edit/:event'
}

const Routes = () => {
    return (
        <Switch>
            <Route exact path={routePaths.home} component={home} />
            <Route path={routePaths.eventEdit} component={eventEdit} />
            <Redirect to={routePaths.home} />
        </Switch>
    )
}

export { routePaths, Routes };