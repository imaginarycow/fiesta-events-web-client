import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import { generateEventId } from '../util/uuid-generator';
import { toast, types } from '../util/toastr';

//actions
import deleteEvent from '../redux/actions/delete-event';
import submitNewEvent from '../redux/actions/submit-new-event';

import '../styles/event-edit.scss';

class EditEvent extends React.Component {

    constructor() {
        super();
        this.state = EditEvent.defaultProps;

        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showDeleteButton = this.showDeleteButton.bind(this);
    }

    componentDidMount() {
        // check if we need to edit existing event or create new
        if (this.props.location.state) {
            // pre-populate the form
            const event = this.props.location.state;
            this.setState({ 
                event: { 
                    id: event.id,
                    date: event.date ? new Date(event.date) : new Date(),
                    name: event.name,
                    location: {
                        name: event.location.name,
                        lat: event.location.lat ? event.location.lat : null,
                        long: event.location.long ? event.location.long : null
                    },
                    price: event.price,
                    start: event.start,
                    stop: event.stop
                },
                showDeleteButton: true,
                submitLabel: 'Update'
            });
        }
    }

    handleChange(e) {
        const target = e.target;
        const name = target.name;
        const val = target.value;
        if (name === 'location') {
            this.setState({ 
                ...this.state,
                event: { ...this.state.event, location: { ...this.state.event.location, name: val } }
            });
        } else {
            this.setState({ 
                ...this.state,
                event: { ...this.state.event, [name]: val }
            });
        }
    }

    handleCancel(e) {
        e.preventDefault();
        // TODO: verify cancel before redirect
        this.setState({ redirect: '/' });
    }

    handleDelete(e) {
        e.preventDefault();
        // eventually verify user intended operation
        this.props.deleteEvent(this.props.user.id, this.state.event.id);
        this.setState(EditEvent.defaultProps);
    }

    handleSubmit(e) {
        e.preventDefault();
        // validate prior to submission
        // TODO: Better input validation
        let event = this.state.event;
        if (event.name.trim() === '' || event.location.name.trim() === '') {
            toast('Event Name and Location are Required!', types.warning);
            return;
        }
        const id = generateEventId(event.name);
        const date = event.date.toLocaleString().split(',')[0];
        event = {...event, date, id}
        this.props.submitNewEvent(this.props.user.id, event);
    }

    showDeleteButton() {
        return (
            <button type="button" className="btn btn-danger" onClick={this.handleDelete}>
                Destroy
            </button>
        )
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }

        const s = this.state.event;

        return (
            <div className="edit-page-container">
                <h1 className='fiesta'>Create | Edit</h1>
                <form>
                    <label>
                        Name
                    </label>
                    <input type='text' name='name' value={s.name} onChange={this.handleChange} maxLength="50"/>
                    
                    <label >
                        Location
                    </label>
                    <input type='text' name='location' value={s.location.name} onChange={this.handleChange} maxLength="50"/>
                    
                    <label>
                        Date
                    </label>
                    <DatePicker
                        onChange={(date) => this.setState({ ...this.state, event: { ...this.state.event, date }})}
                        value={s.date}
                    />
                    
                    <label>
                        Start
                    </label>
                    <TimePicker
                        onChange={(time) => this.setState({...this.state, event: {...this.state.event, start: time}})}
                        value={s.start}
                    />
                    
                    <label>
                        Stop
                    </label>
                    <TimePicker
                        onChange={(time) => this.setState({...this.state, event: {...this.state.event, stop: time}})}
                        value={s.stop}
                    />
                    
                    <label>
                        Price
                    </label>
                    <input type='number' name='price' value={s.price.toFixed(2)} onChange={this.handleChange}/>
                    
                </form>
                
                <div className='button-container'>

                    {this.state.showDeleteButton ? this.showDeleteButton() : null}

                    <button type="button" className="btn btn-secondary" onClick={this.handleCancel}>
                        Exit / Cancel
                    </button>

                    <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>
                        {this.state.submitLabel}
                    </button>
                    
                </div>
                    
            </div>
        )
    }
    
}

EditEvent.defaultProps = {
    event: {
        date: new Date(),
        name: '',
        location: {
            name: '',
            lat: null,
            long: null
        },
        price: 0.00,
        start: '12:00',
        stop: '12:00'
    },
    redirect: null,
    showDeleteButton: false,
    submitLabel: 'Submit'
}

const mapStateToProps = state => {
    return { events: state.events, user: state.user };
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({deleteEvent, submitNewEvent}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEvent);