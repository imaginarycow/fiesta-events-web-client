import React from 'react';
import EventsContainer from '../components/events/list-container';


const Home = () => {

    // Home will eventually hold more than just the event list
    // and will then need to be converted to a class
    return (
        <EventsContainer/>
    )
    
}

export default Home;