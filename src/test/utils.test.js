import { assert } from 'chai';
import User from '../data/model-user';
import { mapAnonUser } from '../util/user-mapper';

describe('Test Utility functions', () => {

    describe('User Mappers', () => {

        it ('test anonymous user mapper w/valid inputs', () => {
            const user = new User(true, 'b@a.com', '1234', 'testUrl');
            const u = mapAnonUser(user);
            assert(u.anonymous === user.anonymous, 'User type was not successfully mapped');
            assert(u.email === user.email, 'User email was not successfully mapped');
            assert(u.id === user.id, 'User id was not successfully mapped');
            assert(u.img === user.img, 'User img was not successfully mapped');
        });
        it ('test anonymous user mapper w/invalid inputs', () => {
            // TODO: actually test w/invalid inputs
            const user = new User(true, 'b@a.com', '1234', 'testUrl');
            const u = mapAnonUser(user);
            assert(u === user, 'User was not successfully mapped');
        });

        // TODO: add tests for all mappers
    })
  
});