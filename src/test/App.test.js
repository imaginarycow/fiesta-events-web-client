import { assert } from 'chai';

describe('some description of the test domain', () => {
  it ('tests some functionality', () => {
    const something = 1;
    assert(something === 1, 'Error message here');
  });
  it ('tests some other functionality', () => {
    const something = 2;
    assert(something === 1, 'Error message here');
  });
});